package biz.epsilonsoft.test002

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btn1 = findViewById<Button>(R.id.button)
        btn1.setOnClickListener {
            val intent = Intent(this,SecondActivity::class.java)
            startActivity(intent)
//            setContentView(R.layout.activity_second)
        }
    }
}
